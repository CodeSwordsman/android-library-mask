package pro.devflagship.mask

import android.os.Parcel
import android.os.Parcelable
import pro.devflagship.mask.helpers.ConstHelper
import pro.devflagship.mask.helpers.ConstHelper.Companion.TAG_EXTENSION
import pro.devflagship.mask.slots.Slot
import java.util.*

class MaskImpl : Mask {

    private var terminated = true
    private var placeholder:Char? = null
    private var showingEmptySlots = false
    private var forbidInputWhenFilled = false
    private var hideHardcodedHead = false

    private var showHardcodedTail = true
    private var slots:SlotsList

    constructor(parcel: Parcel?)
    {
        this.terminated = parcel!!.readByte() != ConstHelper.ZERO_DIGIT.toByte()
        this.placeholder = parcel.readSerializable() as Char
        this.showingEmptySlots = parcel.readByte() != ConstHelper.ZERO_DIGIT.toByte()
        this.forbidInputWhenFilled = parcel.readByte() != ConstHelper.ZERO_DIGIT.toByte()
        this.hideHardcodedHead = parcel.readByte() != ConstHelper.ZERO_DIGIT.toByte()
        this.showHardcodedTail = parcel.readByte() != ConstHelper.ZERO_DIGIT.toByte()
        this.slots = parcel.readParcelable(SlotsList::class.java.classLoader)!!

    }

    constructor(slots:Array<Slot>,terminated: Boolean)
    {
        this.terminated = terminated

        this.slots = SlotsList.ofArray(slots)

        if(this.slots.size() == ConstHelper.ONE_DIGIT)
        {
            if(!terminated)
            {
                extendTail(ConstHelper.ONE_DIGIT)
            }
        }
    }

    constructor(mask:MaskImpl):this(mask,mask.terminated)

    constructor(mask:MaskImpl, terminated:Boolean)
    {
        this.terminated = terminated
        this.placeholder  = mask.placeholder
        this.showingEmptySlots  = mask.showingEmptySlots
        this.forbidInputWhenFilled = mask.forbidInputWhenFilled
        this.hideHardcodedHead  = mask.hideHardcodedHead
        this.showHardcodedTail  = mask.showHardcodedTail
        this.slots = SlotsList(mask.slots)
    }

    override fun toString(): String {
        return toString(true)
    }

    override fun toUnformattedString(): String {
        return toString(false)
    }

    fun toString(allowDecoration: Boolean): String {
        return if (!slots.isEmpty()) {toStringFrom(slots.getFirstSlot(), allowDecoration)} else {ConstHelper.EMPTY_STRING}
    }

    override fun iterator(): Iterator<Slot?> {
        return slots.iterator()
    }

    private fun toStringFrom(startSlot: Slot?, allowDecoration: Boolean): String {
        val result = StringBuilder()
        var slot = startSlot
        var index = ConstHelper.ZERO_DIGIT
        while (slot != null) {
            var c = slot.getValue()
            if (allowDecoration || !slot.hasTag(Slot.TAG_DECORATION))
            {
                val anyInputFromHere: Boolean = slot.anyInputToTheRight()
                if (!anyInputFromHere && !showingEmptySlots)
                {
                    if (!showHardcodedTail || !slots.checkIsIndex(slot.hardcodedSequenceEndIndex() - ConstHelper.ONE_DIGIT + index))
                    {
                        break
                    }
                    if (c == null && (showingEmptySlots || anyInputFromHere)) {
                        c = getPlaceholder()!!
                    } else if (c == null) {
                        break
                    }
                }
                result.append(c)
            }
            slot = slot.getNextSlot()
            index++
        }
        return result.toString()
    }

    override fun getInitialInputPosition(): Int {
        var cursorPosition = ConstHelper.ZERO_DIGIT
        var slot = slots.getSlot(cursorPosition)
        while (slot?.getValue() != null)
        {
            cursorPosition++
            slot = slot.getNextSlot()!!
        }
        return cursorPosition
    }

    override fun hasUserInput(): Boolean {
        if (slots.isEmpty()) {
            return false
        }

        return slots.getFirstSlot()!!.anyInputToTheRight()
    }

    override fun filled(): Boolean {
        return !slots.isEmpty() && filledFrom(slots.getFirstSlot()!!)
    }

    private fun filledFrom(initialSlot: Slot?) : Boolean{

        if (initialSlot == null) {
            throw IllegalArgumentException(ConstHelper.NULL_FIRST_SLOT_MESSAGE)
        }
        var nextSlot: Slot? = initialSlot
        do {
            if (!nextSlot!!.hasTag(TAG_EXTENSION)) {
                if (!nextSlot.hardcoded() && nextSlot.getValue() == null) {
                    return false
                }
            }
            nextSlot = nextSlot.getNextSlot()!!
        } while (nextSlot != null)

        return true
    }

    override fun clear() {
        slots.clear()
        trimTail()
    }

    override fun insertAt(position: Int, input: CharSequence?, cursorAfterTrailingHardcoded: Boolean): Int {
        if (slots.isEmpty() || !slots.checkIsIndex(position) || input == null || input.count() == ConstHelper.ZERO_DIGIT) {
            return position
        }

        showHardcodedTail = true

        var cursorPosition= position
        var slotCandidate: Slot? = slots.getSlot(position)!!

        if (forbidInputWhenFilled && filledFrom(slotCandidate)) {
            return position
        }

        val inStack: Deque<Char>? = dequeFrom(input)
        while (!inStack!!.isEmpty())
        {
            val newValue: Char? = inStack.pop()
            val slotForInputIndex= validSlotIndexOffset(slotCandidate,newValue)

            if(!showingEmptySlots && slotForInputIndex.nonHarcodedSlotSkipped)
            {
                break
            }

            cursorPosition += slotForInputIndex.indexOffset
            val slotForInput = slots.getSlot(cursorPosition)

            if(slotForInput != null)
            {
                slotCandidate = slotForInput
                val insertOffset = slotCandidate.setValue(newValue,slotForInputIndex.indexOffset > ConstHelper.ZERO_DIGIT)

                cursorPosition += insertOffset
                slotCandidate = slots.getSlot(cursorPosition)

                if(!terminated && emptySlotsOnTail() < ConstHelper.ONE_DIGIT)
                {
                    extendTail(ConstHelper.ONE_DIGIT)
                }
            }
        }

        if(cursorAfterTrailingHardcoded)
        {
            var hardcodedTailLength = ConstHelper.ZERO_DIGIT
            if(slotCandidate != null)
            {
                hardcodedTailLength = slotCandidate.hardcodedSequenceEndIndex()
            }

            if(hardcodedTailLength > ConstHelper.ZERO_DIGIT)
            {
                cursorPosition += hardcodedTailLength
            }
        }

        val nextSlot = slots.getSlot(cursorPosition)
        showHardcodedTail = nextSlot == null || !nextSlot.anyInputToTheRight()

        return cursorPosition
    }

    private fun emptySlotsOnTail():Int
    {
        var count = ConstHelper.ZERO_DIGIT
        var slot = slots.getLastSlot()
        while(slot != null && slot.getValue() == null)
        {
            count++
            slot = slot.getPrevSlot()
        }

        return count
    }

    override fun insertAt(position: Int, input: CharSequence?): Int {
        return insertAt(position,input,true)
    }

    override fun insertFront(input: CharSequence?): Int {
        return insertAt(ConstHelper.ZERO_DIGIT, input, true)
    }

    override fun removeBackwards(position: Int, count: Int): Int {
        return removeBackwardsInner(position, count, true)
    }

    override fun removeBackwardsWithoutHardcoded(position: Int, count: Int): Int {
        return removeBackwardsInner(position, count, false)
    }

    override fun getSize(): Int {
        return slots.size()
    }

    override fun isShowingEmptySlots(): Boolean {
        return showingEmptySlots
    }

    override fun setShowingEmptySlots(showingEmptySlots: Boolean) {
        this.showingEmptySlots = showingEmptySlots
    }

    override fun getPlaceholder(): Char? {
        return if(placeholder != null){placeholder}else{ConstHelper.UNDERSCORE_CHAR}
    }

    override fun setPlaceholder(placeholder: Char?) {
        if(placeholder == null)
        {
           throw IllegalArgumentException(ConstHelper.PLACEHOLDER_NULL_MESSAGE)
        }

        this.placeholder = placeholder
    }

    override fun isHideHardcodedHead(): Boolean {
        return hideHardcodedHead
    }

    override fun setHideHardcodedHead(shouldHideHardcodedHead: Boolean) {
        this.hideHardcodedHead = shouldHideHardcodedHead

        if(!hasUserInput())
        {
            showHardcodedTail = !hideHardcodedHead
        }
    }

    override fun isForbidInputWhenFilled(): Boolean {
        return forbidInputWhenFilled
    }

    override fun setForbidInputWhenFilled(forbidInputWhenFilled: Boolean) {
        this.forbidInputWhenFilled = forbidInputWhenFilled
    }

    override fun findCursorPositionInUnformattedString(cursorPosition: Int): Int {
        if(cursorPosition == ConstHelper.ZERO_DIGIT)
        {
            return ConstHelper.ZERO_DIGIT
        }
        else if(cursorPosition < ConstHelper.ZERO_DIGIT || getSize() < cursorPosition)
        {
            throw IndexOutOfBoundsException(String.format(Locale.getDefault(),ConstHelper.OUTPUT_FORMAT_MASK_IMPL,getSize(),cursorPosition))
        }

        var slot = if(cursorPosition == getSize()) {
            slots.getLastSlot()
        } else {
            slots.getSlot(cursorPosition)
        }

        var privateCursorPosition = cursorPosition

        do
        {
            if(slot!!.hasTag(Slot.TAG_DECORATION))
            {
                privateCursorPosition--
            }
            slot = slot.getPrevSlot()
        } while (slot != null)

        return privateCursorPosition
    }

    fun isTerminated():Boolean
    {
        return terminated
    }

    private fun validSlotIndexOffset(slot:Slot?, value:Char?):SlotIndexOffset
    {
        val result = SlotIndexOffset()
        var privateSlot = slot

        while (privateSlot!= null && !privateSlot.canInsertHere(value))
        {
            if(!result.nonHarcodedSlotSkipped && !privateSlot.hardcoded())
            {
                result.nonHarcodedSlotSkipped = true
            }
            privateSlot = privateSlot.getNextSlot()
            result.indexOffset++
        }

        return result
    }

    private fun extendTail(count:Int)
    {
        var privateCount = count
        if(terminated || privateCount < ConstHelper.ONE_DIGIT)
        {
            return
        }

        while (--privateCount > ConstHelper.ZERO_DIGIT)
        {
            val inserted = slots.insertSlotAt(slots.size(),slots.getLastSlot())
            inserted.setValue(null)
            inserted.withTags(ConstHelper.TAG_EXTENSION)
        }
    }

    private fun trimTail()
    {
        if (terminated || slots.isEmpty())
        {
            return
        }

        var currentSlot = slots.getLastSlot()
        var prevSlot = currentSlot?.getPrevSlot()
        while (isAllowedToRemoveSlot(currentSlot!!,prevSlot!!))
        {
            slots.removeSlotAt(slots.size() - ConstHelper.ONE_DIGIT)
            currentSlot = prevSlot
            prevSlot = prevSlot.getPrevSlot()
        }
    }

    private fun isAllowedToRemoveSlot(removalCandidate:Slot,previousSlot:Slot ):Boolean
    {
        return removalCandidate.hasTag(TAG_EXTENSION) &&
                previousSlot.hasTag(TAG_EXTENSION) &&
                removalCandidate.getValue() == null &&
                previousSlot.getValue() == null
    }

    private fun dequeFrom(char: CharSequence?): Deque<Char>?{
        if (char == null)
        {
            return null
        }

        val out: Deque<Char> = ArrayDeque(char.count())

        for (i in char.count() - ConstHelper.ONE_DIGIT downTo ConstHelper.ZERO_DIGIT) {
            out.push(char.elementAt(i))
        }
        return out
    }

    private fun removeBackwardsInner(position: Int, count: Int, removeHardcoded: Boolean):Int
    {
        var cursorPosition = position
        for (i:Int in ConstHelper.ZERO_DIGIT..(count - ConstHelper.ONE_DIGIT))
        {
            if (slots.checkIsIndex(cursorPosition))
            {
                val s: Slot? = slots.getSlot(cursorPosition)
                if (s != null && (!s.hardcoded() || (removeHardcoded && count == ConstHelper.ONE_DIGIT)))
                {
                    cursorPosition += s.setValue(null)
                }
            }
            cursorPosition--
        }
        cursorPosition++
        trimTail()
        var tmpPosition = cursorPosition
        var slot: Slot?
        do
        {
            slot = slots.getSlot(--tmpPosition)
        } while (slot != null && slot.hardcoded() && tmpPosition > ConstHelper.ZERO_DIGIT)
        showHardcodedTail = tmpPosition <= ConstHelper.ZERO_DIGIT && !hideHardcodedHead

        if (tmpPosition > ConstHelper.ZERO_DIGIT){
            cursorPosition = if (slots.checkIsIndex(position) && slots.getSlot(position)!!.hardcoded() && count == ConstHelper.ONE_DIGIT) {
                tmpPosition
            } else {
                tmpPosition + ConstHelper.ONE_DIGIT
            }
        }
        return if (cursorPosition >= ConstHelper.ZERO_DIGIT && cursorPosition <= slots.size()){cursorPosition} else {ConstHelper.ZERO_DIGIT}
    }

    override fun describeContents(): Int {
        return ConstHelper.ZERO_DIGIT
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeByte(if(this.terminated){ConstHelper.ONE_DIGIT.toByte()}else{ConstHelper.ZERO_DIGIT.toByte()})
        dest?.writeSerializable(this.placeholder)
        dest?.writeByte(if(this.showingEmptySlots){ConstHelper.ONE_DIGIT.toByte()}else{ConstHelper.ZERO_DIGIT.toByte()})
        dest?.writeByte(if(this.forbidInputWhenFilled){ConstHelper.ONE_DIGIT.toByte()}else{ConstHelper.ZERO_DIGIT.toByte()})
        dest?.writeByte(if(this.hideHardcodedHead){ConstHelper.ONE_DIGIT.toByte()}else{ConstHelper.ZERO_DIGIT.toByte()})
        dest?.writeByte(if(this.showHardcodedTail){ConstHelper.ONE_DIGIT.toByte()}else{ConstHelper.ZERO_DIGIT.toByte()})
        dest?.writeParcelable(this.slots,flags)
    }



    companion object CREATOR : Parcelable.Creator<MaskImpl>
    {
        override fun createFromParcel(parcel: Parcel): MaskImpl {
            return MaskImpl(parcel)
        }

        override fun newArray(size: Int): Array<MaskImpl> {
            return MaskImpl.newArray(size)
        }

        @JvmStatic
        fun createTerminated(slots: Array<Slot>):MaskImpl
        {
            return MaskImpl(slots,true)
        }

        @JvmStatic
        fun createNonTerminated(slots: Array<Slot>):MaskImpl
        {
            return MaskImpl(slots,false)
        }
    }

    private class SlotIndexOffset
    {
        var indexOffset: Int = ConstHelper.ZERO_DIGIT
        var nonHarcodedSlotSkipped = false
    }

}
