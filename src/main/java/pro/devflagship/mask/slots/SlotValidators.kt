package pro.devflagship.mask.slots

import pro.devflagship.mask.helpers.ConstHelper
import java.util.*

class SlotValidators
{
    companion object
    {
        class GenerousValidator : Slot.SlotValidator
        {
            override fun validate(value: Char?): Boolean {
                return true
            }
            override fun equals(other: Any?): Boolean {
                return (other != null && other is GenerousValidator)
            }

            override fun hashCode(): Int
            {
                return ConstHelper.GENEROUS_VALIDATOR_HASH
            }
        }

        open class DigitValidator : Slot.SlotValidator
        {
            override fun validate(value: Char?): Boolean {
                return value!!.isDigit()
            }
            override fun equals(other: Any?): Boolean {
                return (other != null && other is DigitValidator)
            }

            override fun hashCode(): Int {
                return ConstHelper.DIGIT_VALIDATOR_HASH
            }
        }

        class MaskedDigitValidator(vararg maskChar: Char) : DigitValidator()
        {
            private var maskChars = DEFAULT_DIGIT_MASK_CHARS

            init {
                if (maskChars.count() == ConstHelper.ZERO_DIGIT)
                {
                    throw IllegalArgumentException(ConstHelper.ZERO_CHAR_MASK_MESSAGE)
                }
                this.maskChars = maskChar.toTypedArray()
            }

            override fun validate(value: Char?): Boolean
            {
                if (super.validate(value)) {
                    return true
                }
                for (aChar in maskChars)
                {
                    if(aChar == value)
                    {
                        return true
                    }
                }
                return false
            }

            override fun equals(other: Any?): Boolean
            {
                if(this === other)
                {
                    return true
                }
                if (other == null || this::class.java != other::class.java) {return false}

                val that = other as MaskedDigitValidator

                return (maskChars.contentEquals(that.maskChars))
            }

            override fun hashCode(): Int {
                return Arrays.hashCode(maskChars)
            }

            companion object
            {
                private val DEFAULT_DIGIT_MASK_CHARS = arrayOf(ConstHelper.UPPERCASE_X_CHAR, ConstHelper.LOWERCASE_X_CHAR, ConstHelper.ASTERISK_CHAR)
            }
        }

        class LetterValidator : Slot.SlotValidator
        {

            private var supportsEnglish:Boolean = false
            private var supportsRussian:Boolean = false

            constructor() : this(true,true)

            constructor(supportsEnglish:Boolean, supportsRussian:Boolean)
            {
                this.supportsEnglish = supportsEnglish
                this.supportsRussian = supportsRussian
            }

            override fun validate(value: Char?): Boolean
            {
                return validateEnglishLetter(value) || validateRussianLetter(value)
            }

            private fun validateEnglishLetter(value: Char?): Boolean
            {
                return supportsEnglish && isEnglishCharacter(value)
            }

            private fun validateRussianLetter(value: Char?): Boolean
            {
                return supportsRussian && isRussianCharacter(value)
            }

            private fun isEnglishCharacter(charCode: Char?): Boolean
            {
                return (charCode in ConstHelper.UPPERCASE_A_CHAR..ConstHelper.UPPERCASE_Z_CHAR) || (charCode in ConstHelper.LOWERCASE_A_CHAR..ConstHelper.LOWERCASE_Z_CHAR)
            }

            private fun isRussianCharacter(charCode: Char?): Boolean
            {
                return charCode in ConstHelper.UPPERCASE_RUS_A_CHAR..ConstHelper.LOWERCASE_RUS_YA_CHAR
            }

            override fun equals(other: Any?): Boolean {
                if(this === other) {return true}
                if(other == null || this::class.java != other::class.java){return false}

                val that = other as LetterValidator
                if(supportsEnglish != that.supportsEnglish){return false}
                return supportsRussian == that.supportsRussian
            }

            override fun hashCode(): Int {
                var result = if(supportsEnglish){ConstHelper.ONE_DIGIT}else{ConstHelper.ZERO_DIGIT}
                result = ConstHelper.HASH_MULTIPLIER_DIGIT * result + if(supportsRussian){ConstHelper.ONE_DIGIT}else{ConstHelper.ZERO_DIGIT}
                return result
            }
        }
    }
}