package pro.devflagship.mask.slots

import java.io.Serializable

interface ValueInterpreter : Serializable
{
    fun interpret(character:Char?):Char
}