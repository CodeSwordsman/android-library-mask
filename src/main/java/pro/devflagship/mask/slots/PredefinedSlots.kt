package pro.devflagship.mask.slots

import pro.devflagship.mask.helpers.ConstHelper

class PredefinedSlots {

    companion object {
        val SINGLE_SLOT:Array<Slot> = arrayOf(PredefinedSlots.any())

        //val RUS_PHONE_NUMBER_LIMITED = arrayOf()
           // hardcodedSlot(ConstHelper.)

        val RUS_PHONE_NUMBER = arrayOf(
            hardcodedSlot(ConstHelper.PLUS_CHAR),
            hardcodedSlot(ConstHelper.FIRST_PHONE_DIGIT_CHAR),
            hardcodedSlot(ConstHelper.LEFT_PARENTHESIS_CHAR).withTags(Slot.TAG_DECORATION),
            digit(),
            digit(),
            digit(),
            hardcodedSlot(ConstHelper.RIGHT_PARENTHESIS_CHAR).withTags(Slot.TAG_DECORATION),
            digit(),
            digit(),
            digit(),
            hardcodedSlot(ConstHelper.MINUS_CHAR).withTags(Slot.TAG_DECORATION),
            digit(),
            digit(),
            hardcodedSlot(ConstHelper.MINUS_CHAR).withTags(Slot.TAG_DECORATION),
            digit(),
            digit()
            )

        val RUS_PASSPORT = arrayOf(
            digit(),
            digit(),
            digit(),
            digit(),
            hardcodedSlot(ConstHelper.SPACE_CHAR),
            digit(),
            digit(),
            digit(),
            digit(),
            digit(),
            digit()
        )

        val CARD_NUMBER_STANDARD = arrayOf(
            digit(),
            digit(),
            digit(),
            digit(),
            hardcodedSlot(ConstHelper.SPACE_CHAR).withTags(Slot.TAG_DECORATION),
            digit(),
            digit(),
            digit(),
            digit(),
            hardcodedSlot(ConstHelper.SPACE_CHAR).withTags(Slot.TAG_DECORATION),
            digit(),
            digit(),
            digit(),
            digit(),
            hardcodedSlot(ConstHelper.SPACE_CHAR).withTags(Slot.TAG_DECORATION),
            digit(),
            digit(),
            digit(),
            digit()
        )
        val CARD_NUMBER_MAESTRO = arrayOf(
            digit(),
            digit(),
            digit(),
            digit(),
            digit(),
            digit(),
            digit(),
            digit(),
            hardcodedSlot(ConstHelper.SPACE_CHAR).withTags(Slot.TAG_DECORATION),
            digit(),
            digit(),
            digit(),
            digit()
        )
        val CARD_NUMBER_STANDARD_MASKABLE = arrayOf(
            digit(),
            maskableDigit(),
            maskableDigit(),
            maskableDigit(),
            hardcodedSlot(ConstHelper.SPACE_CHAR).withTags(Slot.TAG_DECORATION),
            maskableDigit(),
            maskableDigit(),
            maskableDigit(),
            maskableDigit(),
            hardcodedSlot(ConstHelper.SPACE_CHAR).withTags(Slot.TAG_DECORATION),
            maskableDigit(),
            maskableDigit(),
            maskableDigit(),
            maskableDigit(),
            hardcodedSlot(ConstHelper.SPACE_CHAR).withTags(Slot.TAG_DECORATION),
            maskableDigit(),
            maskableDigit(),
            maskableDigit(),
            maskableDigit()
        )
        val CARD_NUMBER_MAESTRO_MASKABLE = arrayOf(
            digit(),
            maskableDigit(),
            maskableDigit(),
            maskableDigit(),
            maskableDigit(),
            maskableDigit(),
            maskableDigit(),
            maskableDigit(),
            hardcodedSlot(ConstHelper.SPACE_CHAR).withTags(Slot.TAG_DECORATION),
            maskableDigit(),
            maskableDigit(),
            maskableDigit(),
            maskableDigit()
        )

        @JvmStatic
        fun replaceableSlot(value:Char): Slot
        {
            return digit()
        }

        @JvmStatic
        fun hardcodedSlot(value:Char):Slot
        {
            return Slot(Slot.RULES_HARDCODED,value,null)
        }

        @JvmStatic
        fun digit():Slot
        {
            return Slot(null, SlotValidators.Companion.DigitValidator())
        }

        @JvmStatic
        fun any(): Slot
        {
            return Slot(null, SlotValidators.Companion.GenerousValidator())
        }

        @JvmStatic
        fun maskableDigit():Slot
        {
           return Slot(null, SlotValidators.Companion.MaskedDigitValidator())
        }
    }
}