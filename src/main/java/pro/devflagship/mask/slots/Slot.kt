package pro.devflagship.mask.slots

import android.os.Parcel
import android.os.Parcelable
import pro.devflagship.mask.helpers.ConstHelper
import java.io.Serializable

class Slot : Serializable, Parcelable
{
    private var rulesFlags = RULES_DEFAULT
    private var value : Char? = null
    private var valueInterpreter : ValueInterpreter? = null
    private var tags : Set<Int> = hashSetOf()
    private var validators:SlotValidatorSet? = null
    private var nextSlot:Slot? = null
    private var prevSlot:Slot? = null

    constructor(rules:Int,value: Char?,validators:SlotValidatorSet?)
    {
        this.rulesFlags = rules
        this.value = value
        this.validators = validators ?: SlotValidatorSet()
    }

    constructor(value: Char?,vararg validators: Slot.SlotValidator?):this(RULES_DEFAULT,value,SlotValidatorSet.setOf(*validators))

    constructor(value:Char) : this(RULES_DEFAULT,value,null)

    constructor():this(RULES_DEFAULT,null,null)

    constructor(slotToCopy:Slot):this(slotToCopy.rulesFlags,slotToCopy.value,slotToCopy.getValidators())
    {
        this.valueInterpreter = slotToCopy.valueInterpreter
        this.tags.plus(slotToCopy.tags)
    }

    fun anyInputToTheRight():Boolean
    {
        if(value != null && !hardcoded())
        {
            return true
        }

        if(nextSlot != null)
        {
            return nextSlot!!.anyInputToTheRight()
        }

        return false
    }

    fun setValue(newValue: Char?):Int
    {
        return setValue(newValue,false)
    }

    fun setValue(newValue: Char?, fromleft: Boolean):Int
    {
       return setValueInner(ConstHelper.ZERO_DIGIT,newValue,fromleft)
    }

    fun setFlags(rulesFlags: Int)
    {
        this.rulesFlags = rulesFlags
    }
    fun getFlags(): Int
    {
        return rulesFlags
    }

    fun setValueInterpreter(valueInterpreter: ValueInterpreter)
    {
        this.valueInterpreter = valueInterpreter
    }

    fun withValueInterpreter(valueInterpreter: ValueInterpreter): Slot
    {
        this.valueInterpreter = valueInterpreter
        return this
    }

    private fun setValueInner(offset:Int, newValue:Char?, fromLeft:Boolean):Int
    {
        val newValueKey = if(valueInterpreter == null){newValue}else{valueInterpreter!!.interpret(newValue!!)}
        if(newValueKey == null)
        {
            removeCurrentValue()
            return if(checkRule(RULE_FORBID_CURSOR_MOVE_LEFT)){ConstHelper.ONE_DIGIT}else{ConstHelper.ZERO_DIGIT}
        }
        return setNewValue(offset,newValue!!,fromLeft)
    }

    fun getValue():Char?
    {
        return value
    }

    fun canInsertHere(newValue: Char?): Boolean
    {
        val newValueChar= if(valueInterpreter == null){
            newValue
        } else {valueInterpreter!!.interpret(newValue)}
        if (hardcoded())
        {
            return value == newValue
        }
        return validate(newValueChar)
    }

    private fun validate(valChar:Char?):Boolean
    {
        return (validators == null || validators!!.validate(valChar))
    }

    fun hardcoded():Boolean
    {
        return (value != null && checkRule(RULE_INPUT_MOVES_INPUT))
    }

    fun hardcodedSequenceEndIndex(): Int
    {
        return hardcodedSequenceEndIndex(ConstHelper.ZERO_DIGIT)
    }

    private fun hardcodedSequenceEndIndex(fromIndex: Int) : Int
    {
        var fromI = fromIndex

        if(hardcoded() && (nextSlot == null || !nextSlot!!.hardcoded()))
        {
            return fromIndex + ConstHelper.ONE_DIGIT
        }
        if (hardcoded() && nextSlot!!.hardcoded())
        {
            return nextSlot!!.hardcodedSequenceEndIndex(++fromI)
        }

        return -ConstHelper.ONE_DIGIT
    }

    private fun setNewValue(offset:Int, newValue: Char, fromleft: Boolean):Int
    {
        var changeCurrent = true

        val forbiddenInputFromLeft = fromleft && checkRule(RULE_INPUT_MOVES_INPUT) && !checkRule(RULE_INPUT_REPLACE)

        if(hardcoded() && !forbiddenInputFromLeft && value!! == newValue)
        {
            return if(checkRule(RULE_FORBID_CURSOR_MOVE_RIGHT)){offset}else{offset+ConstHelper.ONE_DIGIT}
        }

        var newOffset = ConstHelper.ZERO_DIGIT

        if(checkRule(RULE_INPUT_MOVES_INPUT) || forbiddenInputFromLeft)
        {
            newOffset = pushValueToSlot(offset+ConstHelper.ONE_DIGIT,newValue,nextSlot)
            changeCurrent = false
        }

        if(value != null && ((rulesFlags.and(MASK_INPUT_RULES) == RULES_DEFAULT)))
        {
            pushValueToSlot(ConstHelper.ZERO_DIGIT,value!!,nextSlot)
        }

        if(changeCurrent)
        {
            value = newValue
            newOffset = if(checkRule(RULE_FORBID_CURSOR_MOVE_RIGHT)){offset}else{offset+ConstHelper.ONE_DIGIT}
        }

        return newOffset
    }

    private fun checkRule(rule:Int):Boolean
    {
        return (rulesFlags.and(rule) == rule)
    }

    private fun removeCurrentValue()
    {
        if(!hardcoded())
        {
            value = pullValueFromSlot(nextSlot)
        }
        else if(prevSlot != null)
        {
            prevSlot!!.removeCurrentValue()
        }
    }

    private fun pushValueToSlot(offset: Int, newValue: Char, slot:Slot?):Int
    {
        if(slot == null)
        {
            return ConstHelper.ZERO_DIGIT
        }

        return nextSlot!!.setValueInner(offset,newValue,true)
    }

    private fun pullValueFromSlot(slot:Slot?):Char?
    {
        if(slot == null)
        {
            return null
        }

        var result:Char? = null

        if(!slot.hardcoded())
        {
            result = slot.getValue()
            if(result != null && !validate(result))
            {
                return null
            }
            slot.removeCurrentValue()
        }
        else if(slot.getNextSlot() != null)
        {
            result = pullValueFromSlot(slot.getNextSlot())
        }
        return result
    }

    fun getNextSlot():Slot?
    {
        return nextSlot
    }

    fun setNextSlot(nextSlot: Slot?)
    {
        this.nextSlot = nextSlot
    }

    fun getPrevSlot(): Slot? {
        return prevSlot
    }

    fun setPrevSlot(prevSlot: Slot?)
    {
        this.prevSlot = prevSlot
    }

    private fun getValidators():SlotValidatorSet?
    {
        return validators
    }

    fun setValidators(validators: SlotValidatorSet?)
    {
        this.validators = validators
    }

    fun getTags() : Set<Int>
    {
        return tags
    }

    fun withTags(vararg tags: Int): Slot
    {
        if (tags.count() == ConstHelper.ZERO_DIGIT)
        {
            return this
        }
        for (tag:Int in tags)
        {
                this.tags.plus(tag)
        }
        return this
    }

    fun hasTag(tag: Int): Boolean
    {
        return tags.contains(tag)
    }

    override fun toString(): String {
        return "${ConstHelper.SLOT_DESC_START}$value${ConstHelper.RIGHT_FIGURE_PARENTHESIS_CHAR}"
    }




    constructor(parcel: Parcel)
    {
        this.rulesFlags = parcel.readInt()
        this.value = parcel.readSerializable() as Char
        this.validators = parcel.readSerializable() as SlotValidatorSet
        this.valueInterpreter = parcel.readSerializable() as ValueInterpreter
        val tagsCount = parcel.readInt()
        for (i:Int in ConstHelper.ZERO_DIGIT ..(tagsCount - ConstHelper.ONE_DIGIT))
        {
            tags.plus(parcel.readInt())
        }

    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(this.rulesFlags)
        dest?.writeSerializable(this.value)
        dest?.writeSerializable(this.validators)
        dest?.writeSerializable(this.valueInterpreter)
        dest?.writeInt(tags.size)

        for (theTag:Int in tags)
        {
            dest?.writeInt(theTag)
        }
    }


    override fun describeContents(): Int {
        return ConstHelper.ZERO_DIGIT
    }

    interface SlotValidator : Serializable
    {
        fun validate(value:Char?):Boolean
    }

    override fun equals(other: Any?): Boolean
    {
        if (this === other){ return true}
        if (other == null || this.javaClass != other.javaClass){return false}
        val slot = other as Slot

        if (rulesFlags != slot.rulesFlags) {return false}
        if (if (value != null){ value != slot.value } else {slot.value != null}) {return false}
        if (if (tags.count() > ConstHelper.ZERO_DIGIT){tags != slot.tags} else{slot.tags.count() > ConstHelper.ZERO_DIGIT}){return false}
        return if (validators != null){validators == slot.validators} else {slot.validators == null}
    }

    override fun hashCode(): Int {
        var result = rulesFlags
        result = ConstHelper.HASH_MULTIPLIER_DIGIT * result + (if (value != null){value.hashCode()} else {ConstHelper.ZERO_DIGIT})
        result = ConstHelper.HASH_MULTIPLIER_DIGIT * result + (if (tags.count() > ConstHelper.ZERO_DIGIT){tags.hashCode()} else {ConstHelper.ZERO_DIGIT})
        result = ConstHelper.HASH_MULTIPLIER_DIGIT * result + (if (validators != null){validators.hashCode()} else {ConstHelper.ZERO_DIGIT})
        return result
    }

    companion object CREATOR : Parcelable.Creator<Slot>
    {
        const val RULE_INPUT_REPLACE = ConstHelper.ONE_DIGIT
        const val RULE_INPUT_MOVES_INPUT = ConstHelper.ONE_DIGIT shl ConstHelper.ONE_DIGIT
        const val MASK_INPUT_RULES = 3
        const val RULES_DEFAULT = ConstHelper.ZERO_DIGIT
        const val RULES_HARDCODED = RULE_INPUT_MOVES_INPUT.or(RULE_INPUT_REPLACE)
        const val RULE_FORBID_CURSOR_MOVE_LEFT = 4
        const val RULE_FORBID_CURSOR_MOVE_RIGHT = 8
        const val TAG_DECORATION = 32

        override fun createFromParcel(parcel: Parcel): Slot {
            return Slot(parcel)
        }

        override fun newArray(size: Int): Array<Slot?> {
            return Array(size) {Slot()}
        }

        @JvmStatic
        fun copySlotArray(arr: Array<Slot>): Array<Slot>
        {
            return arr
        }
    }
}