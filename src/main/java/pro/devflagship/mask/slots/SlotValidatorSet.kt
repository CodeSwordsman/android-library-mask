package pro.devflagship.mask.slots

import pro.devflagship.mask.helpers.ConstHelper

class SlotValidatorSet : HashSet<Slot.SlotValidator>, Slot.SlotValidator
{
    constructor() : super()

    constructor(capacity:Int) : super(capacity)


    fun countValidIn(chars:Collection<Char>?) : Int
    {
        if (chars == null)
        {
            throw IllegalArgumentException(ConstHelper.NULL_STRING_MESSAGE)
        }

        var result = ConstHelper.ZERO_DIGIT
        for(c:Char? in chars)
        {
            if(c == null)
            {
                throw NullPointerException(ConstHelper.NULL_COLLECTION_ELEMENTS_MESSAGE)
            }

            if(validate(c))
            {
                result++
                SlotValidators.Companion.GenerousValidator()
            }
        }
        return result
    }

    override fun validate(value:Char?) : Boolean
    {
        for(validator:Slot.SlotValidator in this)
        {
            if(validator.validate(value))
            {
                return true
            }
        }
        return false
    }

    companion object
    {
        fun setOf(vararg validators:Slot.SlotValidator?) : SlotValidatorSet
        {
            if(validators.count() == ConstHelper.ZERO_DIGIT)
            {
                return SlotValidatorSet()
            }

            val result = SlotValidatorSet(validators.count())
            for(one:Slot.SlotValidator? in validators)
            {
                if(one is SlotValidatorSet)
                {
                    result.addAll(one)
                }
                else
                {
                    result.add(one!!)
                }
            }
            return result
        }
    }
}