package pro.devflagship.mask

import android.os.Parcel
import android.os.Parcelable
import pro.devflagship.mask.helpers.ConstHelper
import pro.devflagship.mask.slots.Slot
import kotlin.IndexOutOfBoundsException


class SlotsList : Iterable<Slot?>,Parcelable
{
    private var size = ConstHelper.ZERO_DIGIT

    private var firstSlot: Slot? = null
    private var lastSlot : Slot? = null

    constructor()

    constructor(list:SlotsList)
    {
        if(!list.isEmpty())
        {
            var previous: Slot? = null
            for (slot in list) {
                val newSlot = Slot(slot!!)
                if (size == ConstHelper.ZERO_DIGIT) {
                    this.firstSlot = newSlot
                } else {
                    previous!!.setNextSlot(newSlot)
                    newSlot.setPrevSlot(previous)
                }

                previous = newSlot
                size++
            }

            this.lastSlot = previous
        }
    }

    fun checkIsIndex(position: Int):Boolean
    {
        return ConstHelper.ZERO_DIGIT <= position && position < size
    }

    fun getSlot(index:Int):Slot?
    {
        if(!checkIsIndex(index))
        {
            return null
        }

        var result:Slot?

        if(index < (size shr ConstHelper.ONE_DIGIT))
        {
            result = firstSlot!!
            for(i:Int in ConstHelper.ZERO_DIGIT..(index-ConstHelper.ONE_DIGIT))
            {
                result = result!!.getNextSlot()
            }
        }
        else
        {
            result = lastSlot
            for(i:Int in (size - ConstHelper.ONE_DIGIT) downTo (index + ConstHelper.ONE_DIGIT))
            {
                result = result!!.getPrevSlot()
            }
        }

        if(result == null)
        {
            throw IllegalStateException(ConstHelper.SLOT_OF_MASK_NULL_MESSAGE)
        }

        return result
    }

    fun insertSlotAt(position: Int, slot:Slot?):Slot
    {
        if(position < ConstHelper.ZERO_DIGIT || size < position)
        {
            IndexOutOfBoundsException(ConstHelper.NEW_POSITION_OUT_OF_SLOTS_LIST_MESSAGE)
        }

        val toInsert = Slot(slot!!)

        val currentSlot = getSlot(position)
        val leftNeighbour:Slot?
        var rightNeighbour:Slot? = null

        if(currentSlot == null)
        {
            leftNeighbour = lastSlot
        }
        else
        {
            leftNeighbour = currentSlot.getPrevSlot()
            rightNeighbour = currentSlot
        }

        toInsert.setNextSlot(rightNeighbour)
        toInsert.setPrevSlot(leftNeighbour)

        rightNeighbour?.setPrevSlot(toInsert)

        leftNeighbour?.setNextSlot(toInsert)

        if(position == ConstHelper.ZERO_DIGIT)
        {
            firstSlot = toInsert
        }
        else if(position == size)
        {
            lastSlot = toInsert
        }

        size++

        return toInsert
    }

    fun removeSlotAt(position: Int):Slot?
    {
        if(!checkIsIndex(position))
        {
            IndexOutOfBoundsException(ConstHelper.POSITION_OUT_OF_SLOTS_LIST_MESSAGE)
        }
        return removeSlot(getSlot(position))
    }

    private fun removeSlot(slotToRemove:Slot?):Slot?
    {
        if(slotToRemove == null || !contains(slotToRemove))
        {
            return null
        }

        val left = slotToRemove.getPrevSlot()
        val right = slotToRemove.getNextSlot()

        if(left != null)
        {
            left.setNextSlot(right)
        }
        else
        {
            firstSlot = right
        }

        if(right != null)
        {
            right.setPrevSlot(left)
        }
        else
        {
            firstSlot = left
        }

        size--

        return slotToRemove
    }

    fun clear()
    {
        if(isEmpty())
        {
            return
        }

        var slot = lastSlot
        while(slot != null)
        {
            slot.setValue(null)
            slot = slot.getPrevSlot()
        }
    }

    fun isEmpty():Boolean
    {
        return size == ConstHelper.ZERO_DIGIT
    }

    override fun iterator(): SlotsIterator {
        return SlotsIterator(firstSlot)
    }

    private fun toArray():Array<Slot>
    {
        if(isEmpty())
        {
            return Array(ConstHelper.ZERO_DIGIT) { Slot() }
        }
        return Array(size) {Slot()}
    }


    fun <T> toArray(array: Array<T>): Array<T>
    {
        lateinit var privateArray:Array<T>
        privateArray = if(array.size < size) {
            java.lang.reflect.Array.newInstance(privateArray::class.java.componentType,size) as Array<T>
        } else {
            array
        }

        var index = ConstHelper.ZERO_DIGIT
        val result:Array<Any> = privateArray as Array<Any>
        for(slot in this)
        {
            result[index++] = slot as Any
        }

        return array
    }

    fun add(slot: Slot): Boolean
    {
        return insertSlotAt(size,slot) == slot
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {return true}
        if (other == null || SlotsList::class.java != other::class.java) {return false}

        val list = other as SlotsList
        if (list.size != size){return false}

        val ourIterator: SlotsIterator = iterator()
            for (otherSlot:Slot? in list) {
                if (ourIterator.next() == otherSlot){
                    return false
                }
            }
        return true
    }

    fun size(): Int
    {
        return size
    }

    fun getFirstSlot(): Slot?
    {
        return firstSlot
    }

    fun setFirstSlot(firstSlot: Slot)
    {
        this.firstSlot = firstSlot
    }

    fun getLastSlot() : Slot?
    {
        return lastSlot
    }

    fun setLastSlot(lastSlot: Slot)
    {
        this.lastSlot = lastSlot
    }

    private fun contains(other : Slot) : Boolean
    {
        for (slot in this)
        {
            if (slot == other){return true}
        }
        return false
    }

    private fun slotsList(parcel: Parcel)
    {
        this.size = parcel.readInt()
        if (size > ConstHelper.ZERO_DIGIT)
        {
            //slots хз как было задать размер без ошибки
            val slots: Array<Slot> = Array(this.size){Slot()}
            parcel.readTypedArray(slots, Slot.CREATOR)
            linkSlots(slots, this)
        }
    }


    constructor(parcel: Parcel) {
        size = parcel.readInt()
        if(size > ConstHelper.ZERO_DIGIT)
        {
            val slots = Array(this.size) {Slot()}
            parcel.readTypedArray(slots, Slot.CREATOR)
            linkSlots(slots,this)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(size)
        if(size > ConstHelper.ZERO_DIGIT)
        {
            parcel.writeTypedArray(toArray(),flags)
        }
    }

    override fun describeContents(): Int {
        return ConstHelper.ZERO_DIGIT
    }

    override fun hashCode(): Int {
        var result = size
        result = ConstHelper.HASH_MULTIPLIER_DIGIT * result + (firstSlot?.hashCode() ?: ConstHelper.ZERO_DIGIT)
        result = ConstHelper.HASH_MULTIPLIER_DIGIT * result + (lastSlot?.hashCode() ?: ConstHelper.ZERO_DIGIT)
        return result
    }

    class SlotsIterator(currentSlot: Slot?) : Iterator<Slot?>
    {
        private var nextSlot:Slot? = currentSlot

        init
        {
            if(currentSlot == null)
            {
                IllegalArgumentException(ConstHelper.INITIAL_SLOT_NULL_MESSAGE)
            }

            this.nextSlot = currentSlot
        }

        override fun hasNext(): Boolean {
            return nextSlot != null
        }

        override fun next(): Slot? {
            val current = nextSlot
            nextSlot = nextSlot?.getNextSlot()
            return current
        }
    }

    companion object CREATOR : Parcelable.Creator<SlotsList> {
        override fun createFromParcel(parcel: Parcel): SlotsList {
            return SlotsList(parcel)
        }

        override fun newArray(size: Int): Array<SlotsList?> {
            return arrayOfNulls(size)
        }

        @JvmStatic
        fun ofArray(slots:Array<Slot>):SlotsList
        {
            val list = SlotsList()

            list.size = slots.count()

            if(list.size == ConstHelper.ZERO_DIGIT)
            {
                return list
            }

            linkSlots(slots,list)

            return list
        }

        @JvmStatic
        fun linkSlots(slots:Array<Slot>,list:SlotsList)
        {
            list.firstSlot = Slot(slots[ConstHelper.ZERO_DIGIT])
            var prev = list.firstSlot

            if(list.size == ConstHelper.ONE_DIGIT)
            {
                list.lastSlot = list.firstSlot
            }

            for(i:Int in ConstHelper.ONE_DIGIT..(slots.count() - ConstHelper.ONE_DIGIT))
            {
                val next = Slot(slots[i])
                prev!!.setNextSlot(next)
                next.setPrevSlot(prev)

                prev = next

                if(i == slots.count() - ConstHelper.ONE_DIGIT)
                {
                    list.lastSlot = next
                }
            }
        }
    }


}