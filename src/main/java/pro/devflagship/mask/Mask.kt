package pro.devflagship.mask

import android.os.Parcelable
import pro.devflagship.mask.slots.Slot


interface Mask : Iterable<Slot?>, Parcelable {
    fun toUnformattedString(): String
    fun getInitialInputPosition(): Int
    fun hasUserInput(): Boolean
    fun filled(): Boolean
    fun clear()
    fun insertAt(position: Int, input: CharSequence?, cursorAfterTrailingHardcoded: Boolean): Int
    fun insertAt(position: Int, input: CharSequence?): Int
    fun insertFront(input: CharSequence?): Int
    fun removeBackwards(position: Int,count: Int): Int
    fun removeBackwardsWithoutHardcoded(position: Int, count: Int) : Int
    fun getSize() : Int
    fun isShowingEmptySlots(): Boolean
    fun setShowingEmptySlots(showingEmptySlots: Boolean)
    fun getPlaceholder() : Char?
    fun setPlaceholder(placeholder: Char?)
    fun isHideHardcodedHead(): Boolean
    fun setHideHardcodedHead(shouldHideHardcodedHead: Boolean)
    fun isForbidInputWhenFilled(): Boolean
    fun setForbidInputWhenFilled(forbidInputWhenFilled: Boolean)
    fun findCursorPositionInUnformattedString(cursorPosition: Int): Int
}