package pro.devflagship.mask

import android.os.Parcel
import android.os.Parcelable
import pro.devflagship.mask.helpers.ConstHelper
import pro.devflagship.mask.slots.PredefinedSlots
import pro.devflagship.mask.slots.Slot
import java.io.Serializable
import java.util.*

class MaskDescriptor : Serializable,Parcelable
{
    private var slots:Array<Slot>? = null

    private var rawMask : String? = null
    private var initialValue:String? = null

    private var terminated = true
    private var forbidInputWhenFilled = false
    private var hideHardcodedHead = true

    constructor()

    constructor(copy:MaskDescriptor)
    {
        if(copy.slots != null)
        {
            this.slots = copy.slots
        }

        this.rawMask = copy.rawMask
        this.initialValue = copy.initialValue

        this.terminated = copy.terminated
        this.hideHardcodedHead = copy.hideHardcodedHead
        this.forbidInputWhenFilled = copy.forbidInputWhenFilled
    }

    constructor(parcel: Parcel)
    {
        this.slots = parcel.createTypedArray(Slot.CREATOR)
        this.rawMask = parcel.readString()
        this.initialValue = parcel.readString()
        this.terminated = (parcel.readByte() != ConstHelper.ZERO_DIGIT.toByte())
        this.forbidInputWhenFilled = (parcel.readByte() != ConstHelper.ZERO_DIGIT.toByte())
        this.hideHardcodedHead = (parcel.readByte() != ConstHelper.ZERO_DIGIT.toByte())
    }

    private fun isValid():Boolean
    {
        return slots != null || !TextUtils.isEmpty(rawMask)
    }

    fun validateOrThrow()
    {
        if(!isValid())
        {
            IllegalStateException(ConstHelper.MAILFORMED_MASK_DESCRIPTOR_MESSAGE)
        }
    }

    fun getSlots():Array<Slot>?
    {
        return slots
    }

    fun setSlots(slots: Array<Slot>?):MaskDescriptor
    {
        this.slots = slots
        return this
    }

    fun getRawMask():String?
    {
        return rawMask
    }

    fun setRawMask(rawMask: String?):MaskDescriptor
    {
        this.rawMask = rawMask
        return this
    }

    fun isTerminated():Boolean
    {
        return terminated
    }

    fun setTerminated(terminated: Boolean):MaskDescriptor
    {
        this.terminated = terminated
        return this
    }

    fun getInitialValue():String?
    {
        return initialValue
    }

    fun setInitialValue(initialValue:String?):MaskDescriptor
    {
        this.initialValue = initialValue
        return this
    }

    fun isForbidInputWhenFilled():Boolean
    {
        return forbidInputWhenFilled
    }

    fun setForbidInputWhenFilled(forbidInputWhenFilled:Boolean):MaskDescriptor
    {
        this.forbidInputWhenFilled = forbidInputWhenFilled
        return this
    }

    fun isHideHardcodedHead():Boolean
    {
        return hideHardcodedHead
    }

    fun setHideHardcodedHead(hideHardcodedHead:Boolean):MaskDescriptor
    {
        this.hideHardcodedHead = hideHardcodedHead
        return this
    }

    override fun writeToParcel(parcel: Parcel, flags: Int)
    {
        parcel.writeTypedArray(this.slots,flags)
        parcel.writeString(this.rawMask)
        parcel.writeString(this.initialValue)
        parcel.writeByte(if(this.terminated){ConstHelper.ONE_DIGIT.toByte()}else{ConstHelper.ZERO_DIGIT.toByte()})
        parcel.writeByte(if(this.forbidInputWhenFilled){ConstHelper.ONE_DIGIT.toByte()}else{ConstHelper.ZERO_DIGIT.toByte()})
        parcel.writeByte(if(this.hideHardcodedHead){ConstHelper.ONE_DIGIT.toByte()}else{ConstHelper.ZERO_DIGIT.toByte()})
    }

    override fun describeContents(): Int {
        return ConstHelper.ZERO_DIGIT
    }

    override fun equals(other: Any?): Boolean {
        if(this === other){return true}
        if(other == null || this::class.java != other::class.java){return false}
        val that = other as MaskDescriptor
        if(terminated != that.terminated){return false}
        if(forbidInputWhenFilled != that.forbidInputWhenFilled){return false}
        if(hideHardcodedHead != that.hideHardcodedHead){return false}
        if(!slots!!.contentEquals(that.slots!!)){return false}
        if(if(rawMask!= null){rawMask == that.rawMask}else{that.rawMask != null}){return false}
        return if(initialValue != null){initialValue == that.initialValue}else{that.initialValue == null}
    }

    override fun hashCode(): Int {
        var result = Arrays.hashCode(slots)
        result = ConstHelper.HASH_MULTIPLIER_DIGIT * result + if(rawMask!=null){rawMask.hashCode()}else{ConstHelper.ZERO_DIGIT}
        result = ConstHelper.HASH_MULTIPLIER_DIGIT * result + if(initialValue!=null){initialValue.hashCode()}else{ConstHelper.ZERO_DIGIT}
        result = ConstHelper.HASH_MULTIPLIER_DIGIT * result + if (terminated){ConstHelper.ONE_DIGIT}else{ConstHelper.ZERO_DIGIT}
        result = ConstHelper.HASH_MULTIPLIER_DIGIT * result + if (forbidInputWhenFilled){ConstHelper.ONE_DIGIT}else{ConstHelper.ZERO_DIGIT}
        result = ConstHelper.HASH_MULTIPLIER_DIGIT * result + if (hideHardcodedHead){ConstHelper.ONE_DIGIT}else{ConstHelper.ZERO_DIGIT}
        return result
    }

    override fun toString(): String {
        if(!TextUtils.isEmpty(rawMask))
        {
            return rawMask!!
        }
        else if(slots != null && slots!!.count() > ConstHelper.ZERO_DIGIT)
        {
            return slotsToString()
        }

        return ConstHelper.EMPTY_SLOTS
    }

    private fun slotsToString():String
    {
        val result = StringBuilder(slots!!.count())
        for(slot in slots!!)
        {
            var value = slot.getValue()
            if(value == null)
            {
                value = ConstHelper.UNDERSCORE_CHAR
            }

            result.append(value)
        }

        return result.toString()
    }

    companion object CREATOR : Parcelable.Creator<MaskDescriptor>
    {
        @JvmStatic
        fun ofRawMask(rawMask:String?) : MaskDescriptor
        {
            if(TextUtils.isEmpty(rawMask))
            {
                return emptyMask()
            }

            return MaskDescriptor().setRawMask(rawMask)
        }

        @JvmStatic
        fun ofRawMask(rawMask: String?, terminated:Boolean) : MaskDescriptor
        {
            return MaskDescriptor().setRawMask(rawMask).setTerminated(terminated)
        }

        @JvmStatic
        fun ofSlots(slots:Array<Slot>?):MaskDescriptor
        {
           return MaskDescriptor().setSlots(slots)
        }

        @JvmStatic
        fun emptyMask():MaskDescriptor
        {
            return MaskDescriptor().setSlots(Array(ConstHelper.ZERO_DIGIT) { PredefinedSlots.any() }).setTerminated(false)
        }

        override fun createFromParcel(parcel: Parcel): MaskDescriptor {
            return MaskDescriptor(parcel)
        }

        override fun newArray(size: Int): Array<MaskDescriptor?> {
            return arrayOfNulls(size)
        }
    }
}