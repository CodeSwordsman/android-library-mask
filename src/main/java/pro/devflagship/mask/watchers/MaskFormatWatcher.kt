package pro.devflagship.mask.watchers

import pro.devflagship.mask.Mask
import pro.devflagship.mask.MaskImpl

class MaskFormatWatcher(maskOriginal: MaskImpl) : FormatWatcher() {
    private var maskOriginal: MaskImpl? = null

    init {
        setMask(maskOriginal)
    }

    override fun createMask(): Mask {
        return MaskImpl(maskOriginal!!)
    }

    fun getMaskOriginal(): Mask? {
        return maskOriginal
    }

    private fun setMask(maskOriginal: MaskImpl) {
        this.maskOriginal = maskOriginal
        refreshMask()
    }

    fun swapMask(newMask: MaskImpl) {
        maskOriginal = MaskImpl(newMask)
        maskOriginal!!.clear()

        refreshMask(newMask.toString())
    }

}