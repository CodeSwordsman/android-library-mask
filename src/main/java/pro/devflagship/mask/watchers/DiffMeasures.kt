package pro.devflagship.mask.watchers

import pro.devflagship.mask.helpers.ConstHelper
import java.util.*

class DiffMeasures {

    private var diffStartPosition: Int = ConstHelper.ZERO_DIGIT
    private var diffInsertLength: Int = ConstHelper.ZERO_DIGIT
    private var diffRemoveLength: Int = ConstHelper.ZERO_DIGIT
    private var diffType: Int = ConstHelper.ZERO_DIGIT
    private var cursorPosition: Int = ConstHelper.ZERO_DIGIT

    private var trimmingSequence: Boolean = false

    fun calculateBeforeTextChanged(start: Int, count: Int, after: Int){
        diffStartPosition = start
        diffRemoveLength = ConstHelper.ZERO_DIGIT
        diffType = ConstHelper.ZERO_DIGIT
        diffInsertLength = ConstHelper.ZERO_DIGIT
        cursorPosition = -ConstHelper.ONE_DIGIT

        if (after > ConstHelper.ZERO_DIGIT)
        {
            diffType = diffType.or(INSERT)
            diffInsertLength = after
        }

        if (count > ConstHelper.ZERO_DIGIT)
        {
            diffType = diffType.or(REMOVE)
            diffRemoveLength = count
        }
        trimmingSequence =
                diffInsertLength > ConstHelper.ZERO_DIGIT
                && diffRemoveLength > ConstHelper.ZERO_DIGIT
                && diffInsertLength < diffRemoveLength
    }

    fun recalculateOnModifyingWord(realDiffLen: Int)
    {
        diffRemoveLength -= diffInsertLength
        diffStartPosition += realDiffLen
        diffType = diffType.and(INSERT.inv())
    }

    fun isInsertingChars(): Boolean {
        return diffType.and(INSERT) == INSERT
    }

    fun isRemovingChars(): Boolean
    {
        return diffType.and(REMOVE) == REMOVE
    }

    fun getInsertEndPosition(): Int
    {
        return diffStartPosition + diffInsertLength
    }

    fun getRemoveEndPosition(): Int
    {
        return diffStartPosition + diffRemoveLength - ConstHelper.ONE_DIGIT
    }

    fun setCursorPosition(cursorPosition: Int)
    {
        this.cursorPosition = cursorPosition
    }

    fun getStartPosition(): Int {
        return diffStartPosition
    }

    fun getDiffInsertLength(): Int {
        return diffInsertLength
    }

    fun getRemoveLength(): Int {
        return diffRemoveLength
    }

    fun getDiffType(): Int {
        return diffType
    }

    fun getCursorPosition(): Int {
        return cursorPosition
    }

    fun isTrimmingSequence(): Boolean {
        return trimmingSequence
    }

    override fun toString(): String {

        var type: String? = null
        when {
            MASK_BOTH_TYPE.and(diffType) == MASK_BOTH_TYPE -> type = ConstHelper.MASK_TYPE_BOTH
            INSERT.and(diffType) == INSERT -> type = ConstHelper.MASK_TYPE_INSERT
            REMOVE.and(diffType) == REMOVE -> type = ConstHelper.MASK_TYPE_REMOVE
            diffType == ConstHelper.ZERO_DIGIT -> type = ConstHelper.MASK_TYPE_NONE
        }

        if (type == null)
        {
            throw IllegalStateException("${ConstHelper.UNKNOWN_DIFFTYPE_MESSAGE}$diffType")
        }

        return String.format(
            Locale.getDefault(),
            ConstHelper.OUTPUT_FORMAT_DIFF_MEASURES,
            type,
            diffStartPosition,
            diffInsertLength,
            diffRemoveLength,
            cursorPosition
        )
    }

    companion object
    {
        private const val INSERT = ConstHelper.ONE_DIGIT
        private const val REMOVE = 2
        private const val MASK_BOTH_TYPE = 3
    }
}