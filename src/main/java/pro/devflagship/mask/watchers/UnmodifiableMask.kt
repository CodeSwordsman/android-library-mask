package pro.devflagship.mask.watchers

import android.os.Parcel
import android.os.Parcelable
import pro.devflagship.mask.Mask
import pro.devflagship.mask.helpers.ConstHelper
import pro.devflagship.mask.slots.Slot

class UnmodifiableMask : Mask {

    private var delegate: Mask?

    constructor(parcel: Parcel) {
        this.delegate = parcel.readParcelable(Mask::class.java.classLoader)
    }

    constructor(delegate: Mask?) {
        this.delegate = delegate
    }

    override fun toString(): String {
        return if(delegate == null){ConstHelper.EMPTY_STRING} else {delegate.toString()}
    }

    override fun toUnformattedString(): String {
        return if(delegate == null){ConstHelper.EMPTY_STRING} else { delegate!!.toUnformattedString()}
    }

    override fun getInitialInputPosition(): Int {
        return if(delegate == null){-ConstHelper.ONE_DIGIT} else {delegate!!.getInitialInputPosition()}
    }

    override fun hasUserInput(): Boolean {
        return delegate != null && delegate!!.hasUserInput()
    }

    override fun filled(): Boolean {
        return delegate != null && delegate!!.filled()
    }

    override fun clear() {
        if (delegate != null) {
            clear()
        }
    }

    override fun insertAt(position: Int, input: CharSequence?, cursorAfterTrailingHardcoded: Boolean): Int {
        throw UnsupportedOperationException()
    }

    override fun insertAt(position: Int, input: CharSequence?): Int {
        throw UnsupportedOperationException()
    }

    override fun insertFront(input: CharSequence?): Int {
        throw UnsupportedOperationException()
    }

    override fun removeBackwards(position: Int, count: Int): Int {
        throw UnsupportedOperationException()
    }

    override fun removeBackwardsWithoutHardcoded(position: Int, count: Int): Int {
        throw UnsupportedOperationException()
    }

    override fun getSize(): Int {
        return if (delegate != null){ConstHelper.ZERO_DIGIT} else {delegate!!.getSize()}
    }

    override fun isShowingEmptySlots(): Boolean {
        return delegate != null && delegate!!.isShowingEmptySlots()
    }

    override fun setShowingEmptySlots(showingEmptySlots: Boolean) {
        if (delegate != null) {
            delegate!!.setShowingEmptySlots(showingEmptySlots)
        }
    }

    override fun getPlaceholder(): Char? {
        return if (delegate == null){ null} else {delegate!!.getPlaceholder()}
    }

    override fun setPlaceholder(placeholder: Char?) {
        if (delegate != null) {
            delegate!!.setPlaceholder(placeholder)
        }
    }

    override fun isHideHardcodedHead(): Boolean {
        return delegate != null && delegate!!.isHideHardcodedHead()
    }

    override fun setHideHardcodedHead(shouldHideHardcodedHead: Boolean) {
        if (delegate != null)
        {
            delegate!!.setHideHardcodedHead(shouldHideHardcodedHead)
        }
    }

    override fun isForbidInputWhenFilled(): Boolean {
        return delegate != null && delegate!!.isForbidInputWhenFilled()
    }

    override fun setForbidInputWhenFilled(forbidInputWhenFilled: Boolean) {
        if (delegate != null) {
            delegate!!.setForbidInputWhenFilled(forbidInputWhenFilled)
        }
    }

    override fun findCursorPositionInUnformattedString(cursorPosition: Int): Int {
        return if (delegate == null){cursorPosition} else {delegate!!.findCursorPositionInUnformattedString(cursorPosition)}
    }

    override fun iterator(): Iterator<Slot?>
    {
        return delegate!!.iterator()
    }

    override fun writeToParcel(dest: Parcel?, flags: Int)
    {
        dest!!.writeParcelable(this.delegate, flags)
    }

    override fun describeContents(): Int
    {
        return ConstHelper.ZERO_DIGIT
    }

    companion object CREATOR : Parcelable.Creator<UnmodifiableMask> {
        override fun createFromParcel(parcel: Parcel): UnmodifiableMask {
            return UnmodifiableMask(parcel)
        }

        override fun newArray(size: Int): Array<UnmodifiableMask?> {
            return arrayOfNulls(size)
        }
    }
}