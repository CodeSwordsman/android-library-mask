package pro.devflagship.mask.watchers

import pro.devflagship.mask.Mask
import pro.devflagship.mask.MaskDescriptor
import pro.devflagship.mask.MaskFactoryImpl
import pro.devflagship.mask.parser.SlotsParser

class DescriptorFormatWatcher : FormatWatcher {

    private var slotsParser: SlotsParser? = null
    private var maskDescriptor: MaskDescriptor? = null

    constructor(slotsParser: SlotsParser?)
    {
        this.slotsParser = slotsParser
    }

    constructor(slotsParser: SlotsParser?, maskDescriptor: MaskDescriptor?)
    {
        this.slotsParser = slotsParser
        this.maskDescriptor = maskDescriptor
        if(maskDescriptor != null)
        {
            changeMask(maskDescriptor)
        }
    }

    constructor() : this(null,MaskDescriptor.emptyMask())

    constructor(maskDescriptor: MaskDescriptor?) : this(null, maskDescriptor)

    fun changeMask(maskDescriptor: MaskDescriptor)
    {
        this.maskDescriptor = maskDescriptor
        refreshMask(maskDescriptor.getInitialValue())
    }



    override fun createMask(): Mask
    {
        return MaskFactoryImpl(slotsParser!!, maskDescriptor!!).createMask()
    }

    fun setSlotsParser(slotsParser: SlotsParser?)
    {
        this.slotsParser = slotsParser
    }

    fun getSlotsParser():SlotsParser?
    {
        return slotsParser
    }

}