package pro.devflagship.mask.watchers

import android.text.TextWatcher
import pro.devflagship.mask.MaskFactory
import pro.devflagship.mask.helpers.ConstHelper
import android.widget.TextView
import pro.devflagship.mask.FormattedTextChangeListener
import pro.devflagship.mask.Mask
import android.text.Editable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.view.inputmethod.BaseInputConnection
import android.widget.EditText
import android.view.View
import android.view.inputmethod.InputMethodManager

abstract class FormatWatcher : TextWatcher, MaskFactory{


    private var diffMeasures = DiffMeasures()
    private var textBeforeChange: CharSequence? = null

    private var mask: Mask? = null
    private var textView: TextView? = null
    private var initWithMask: Boolean = false

    private var selfEdit = false
    private var noChanges = false
    private var formattingCancelled = false
    private var callback: FormattedTextChangeListener? = null




    fun installOn(textView: TextView, forwardHardcoded:Boolean = false)
    {
        installOn(textView, false, forwardHardcoded)
    }

    fun installOnAndFill(textView: TextView)
    {
        installOn(textView, true)
    }

    fun removeFromTextView() {
        if (textView != null) {
            this.textView!!.removeTextChangedListener(this)
            this.textView = null
        }
    }

    private fun isInstalled(): Boolean {
        return this.textView != null
    }

    fun hasMask(): Boolean {
        return mask != null
    }

    private fun installOn(textView: TextView, initWithMask: Boolean, forwardHardcoded:Boolean = false)
    {
        this.textView = textView
        this.initWithMask = initWithMask

        textView.removeTextChangedListener(this)
        textView.addTextChangedListener(this)

        this.mask = null
        refreshMask()
        if(forwardHardcoded)
        {
            afterTextChanged(textView.text as Editable)
        }

    }

    fun refreshMask() {
        refreshMask(null)
    }

    fun refreshMask(initialValue: CharSequence?)
    {
        val initial = this.mask == null
        this.mask = createMask()
        checkMask()

        val initiationNeeded = initialValue != null
        diffMeasures = DiffMeasures()

        if(initiationNeeded)
        {
            diffMeasures.setCursorPosition(mask!!.insertFront(initialValue!!))
        }

        if((!initial || initWithMask || initiationNeeded) && isInstalled())
        {
            selfEdit = true
            val formattedInitialValue = mask.toString()
            if(textView is EditText)
            {
                val editable:Editable = textView!!.text as Editable
                editable.replace(ConstHelper.ZERO_DIGIT,editable.length,formattedInitialValue,ConstHelper.ZERO_DIGIT, formattedInitialValue.length)
            }
            else
            {
                textView!!.text = formattedInitialValue
            }

            setSelection(this.mask!!.getInitialInputPosition())
            selfEdit = false
        }
    }

    override fun toString(): String {
        return if(mask == null){ConstHelper.EMPTY_STRING} else {mask.toString()}
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        if (selfEdit || mask == null) {
            return
        }
        textBeforeChange = s.toString()
        diffMeasures.calculateBeforeTextChanged(start,count,after)
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if (selfEdit || mask == null) {
            return
        }

        var diffChars: CharSequence? = null
        if (diffMeasures.isInsertingChars()) {
            diffChars = s!!.subSequence(diffMeasures.getStartPosition(), diffMeasures.getInsertEndPosition())
            if (diffMeasures.isTrimmingSequence()) {
                val diffBefore: CharSequence = textBeforeChange!!.subSequence(
                    diffMeasures.getStartPosition(),
                    diffMeasures.getInsertEndPosition()
                )
                if (diffBefore == diffChars) {
                    diffMeasures.recalculateOnModifyingWord(diffChars.length)
                }
            }
        }
        if (callback != null && callback!!.beforeFormatting(textBeforeChange.toString(), s.toString())) {
            formattingCancelled = true
            return
        }

        val noChanges = textBeforeChange == s.toString()
        if (noChanges) {
            return
        }
        if (diffMeasures.isRemovingChars()) {
            if (!diffMeasures.isInsertingChars()) {
                diffMeasures.setCursorPosition(
                    mask!!.removeBackwards(
                        diffMeasures.getRemoveEndPosition(),
                        diffMeasures.getRemoveLength()
                    )
                )
            } else {
                diffMeasures.setCursorPosition(
                    mask!!.removeBackwardsWithoutHardcoded(
                        diffMeasures.getRemoveEndPosition(),
                        diffMeasures.getRemoveLength()
                    )
                )
            }
        }
        if (diffMeasures.isInsertingChars())
        {
            diffMeasures.setCursorPosition(mask!!.insertAt(diffMeasures.getStartPosition(), diffChars!!))
        }
    }

    override fun afterTextChanged(newText: Editable?) {
        if (formattingCancelled || selfEdit || mask == null || noChanges) {
            formattingCancelled = false
            noChanges = false
            return
        }
        val formatted = mask.toString()
        val cursorPosition: Int = diffMeasures.getCursorPosition()

        if (formatted != newText.toString()) //if (!formatted.equals(newText.toString()))
        {
            val start = BaseInputConnection.getComposingSpanStart(newText)
            val end = if (cursorPosition > newText!!.length){newText.length} else {cursorPosition}
            val pasted: CharSequence
            pasted = if (start == -ConstHelper.ONE_DIGIT || end == -ConstHelper.ONE_DIGIT)
            {
                formatted
            }
            else
            {
                val sb = SpannableStringBuilder()
                sb.append(formatted.substring(ConstHelper.ZERO_DIGIT, start))
                val composing = SpannableString(formatted.substring(start, end))
                BaseInputConnection.setComposingSpans(composing)
                sb.append(composing)
                sb.append(formatted.substring(end, formatted.length))
                sb
            }
            selfEdit = true

            newText.replace(ConstHelper.ZERO_DIGIT, newText.length, pasted, ConstHelper.ZERO_DIGIT, formatted.length)
            selfEdit = false
        }
        if (ConstHelper.ZERO_DIGIT <= cursorPosition && cursorPosition <= newText!!.length) {
            setSelection(cursorPosition)
        }
        textBeforeChange = null
        if (callback != null) {
            callback!!.onTextFormatted(this, toString())
        }
    }

    fun getMask(): Mask {
        return UnmodifiableMask(mask)
    }

    fun isAttachedTo(view: View): Boolean {
        return this.textView == view
    }

    fun setCallback(callback: FormattedTextChangeListener) {
        this.callback = callback
    }

    fun getCursorPosition(): Int {
        return diffMeasures.getCursorPosition()
    }

    protected fun getTrueMask(): Mask? {
        return mask
    }

    protected fun setTrueMask(mask: Mask) {
        this.mask = mask
    }

    protected fun getTextView(): TextView? {
        return textView
    }

    fun getTextViewPublic():TextView?
    {
        return getTextView()
    }

    protected fun setTextView(textView: TextView) {
        this.textView = textView
    }


    private fun checkMask()
    {
        if (mask == null)
        {
            throw IllegalStateException(ConstHelper.MASK_NULL_AT_POINT_MESSAGE)
        }
    }

    private fun setSelection(position: Int) {
        if (textView is EditText && position <= textView!!.length()) {
            (textView as EditText).setSelection(position)
        }
    }

    companion object
    {
        const val DEBUG = false
        const val TAG = ConstHelper.TAG_FORMAT_WATCHER
    }
}