package pro.devflagship.mask.helpers

internal class ConstHelper
{
    companion object {
        const val NULL_STRING_MESSAGE = "String to validate cannot be null"
        const val NULL_COLLECTION_ELEMENTS_MESSAGE = "We don't support collections with null elements"
        const val ZERO_CHAR_MASK_MESSAGE = "Mask chars cannot be null"
        const val REPRESENTATION_EMPTY_MESSAGE = "String representation of the mask's slots is empty"
        const val SLOT_OF_MASK_NULL_MESSAGE = "Slot inside the mask should not be null. But it is."
        const val NEW_POSITION_OUT_OF_SLOTS_LIST_MESSAGE = "New slot position should be inside the slots list. Or on the tail (position = size)"
        const val POSITION_OUT_OF_SLOTS_LIST_MESSAGE = "Slot position should be inside the slots list"
        const val INITIAL_SLOT_NULL_MESSAGE = "Initial slot for iterator cannot be null"
        const val MAILFORMED_MASK_DESCRIPTOR_MESSAGE = "Mask descriptor is malformed. Should have at least slots array or raw mask (string representation)"
        const val NULL_MASK_DESCRIPTOR_MESSAGE = "MaskDescriptor cannot be null"
        const val NO_SLOTSPARSER_NO_SLOTS_MESSAGE = "Cannot create mask: neither slots nor slots parser and raw-mask are set"
        const val UNKNOWN_DIFFTYPE_MESSAGE = "unknown behaviour for diffType "
        const val NULL_TEXTVIEW_MESSAGE = "text view cannot be null"
        const val MASK_NULL_AT_POINT_MESSAGE = "Mask cannot be null at this point. Check maybe you forgot to call refreshMask()"
        const val NULL_FIRST_SLOT_MESSAGE = "first slot is null"
        const val PLACEHOLDER_NULL_MESSAGE = "Placeholder is null"

        const val SLOT_DESC_START = "Slot{ value="
        const val EMPTY_SLOTS = "(empty)"
        const val TAG_FORMAT_WATCHER = "FormatWatcher"
        const val EMPTY_STRING = ""

        const val MASK_TYPE_NONE = "none"
        const val MASK_TYPE_REMOVE = "remove"
        const val MASK_TYPE_INSERT = "insert"
        const val MASK_TYPE_BOTH = "both"

        const val OUTPUT_FORMAT_DIFF_MEASURES = "[ DiffMeasures type=%s, diffStartPosition=%d, diffInsertLength=%d, diffRemoveLength=%d, cursor: %d ]"
        const val OUTPUT_FORMAT_MASK_IMPL = "Mask size: %d, passed index: %d"

        const val ZERO_DIGIT = 0
        const val ONE_DIGIT = 1
        const val HASH_MULTIPLIER_DIGIT = 31

        const val ASTERISK_CHAR = '*'
        const val LOWERCASE_X_CHAR = 'x'
        const val UPPERCASE_X_CHAR = 'X'
        const val UPPERCASE_A_CHAR = 'A'
        const val LOWERCASE_A_CHAR = 'a'
        const val UPPERCASE_Z_CHAR = 'Z'
        const val LOWERCASE_Z_CHAR = 'z'
        const val UPPERCASE_RUS_A_CHAR = 'А'
        const val LOWERCASE_RUS_YA_CHAR = 'я'
        const val RIGHT_FIGURE_PARENTHESIS_CHAR = '}'
        const val PLUS_CHAR = '+'
        const val FIRST_PHONE_DIGIT_CHAR = '7'
        const val RIGHT_PARENTHESIS_CHAR = ')'
        const val LEFT_PARENTHESIS_CHAR = '('
        const val SPACE_CHAR = ' '
        const val MINUS_CHAR = '-'
        const val UNDERSCORE_CHAR = '_'

        const val GENEROUS_VALIDATOR_HASH = -56328
        const val DIGIT_VALIDATOR_HASH = -56329

        const val TAG_EXTENSION = -149635
    }
}