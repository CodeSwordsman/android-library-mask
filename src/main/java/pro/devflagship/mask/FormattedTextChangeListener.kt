package pro.devflagship.mask

import pro.devflagship.mask.watchers.FormatWatcher

interface FormattedTextChangeListener {
    fun beforeFormatting(oldValue: String, newValue: String): Boolean
    fun onTextFormatted(formatter: FormatWatcher, newFormattedText: String)
}