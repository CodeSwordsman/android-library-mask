package pro.devflagship.mask

interface MaskFactory {
    fun createMask(): Mask
}