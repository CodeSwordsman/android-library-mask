package pro.devflagship.mask

import pro.devflagship.mask.helpers.ConstHelper
import pro.devflagship.mask.parser.SlotsParser

class MaskFactoryImpl(private var slotsParser: SlotsParser?, private var maskDescriptor: MaskDescriptor?) : MaskFactory
{

    override fun createMask(): Mask
    {
        if(maskDescriptor == null)
        {
            throw IllegalArgumentException(ConstHelper.NULL_MASK_DESCRIPTOR_MESSAGE)
        }

        maskDescriptor!!.validateOrThrow()

        if(maskDescriptor!!.getSlots() == null && slotsParser == null)
        {
            throw IllegalStateException(ConstHelper.NO_SLOTSPARSER_NO_SLOTS_MESSAGE)
        }

        val slots = if(maskDescriptor!!.getSlots() != null){maskDescriptor!!.getSlots()}else{slotsParser!!.parseSlots(maskDescriptor!!.getRawMask()!!)}

        val mask = MaskImpl(slots!!, maskDescriptor!!.isTerminated())
        mask.setForbidInputWhenFilled(maskDescriptor!!.isForbidInputWhenFilled())
        mask.setHideHardcodedHead(maskDescriptor!!.isHideHardcodedHead())

        return mask
    }
}