package pro.devflagship.mask

class TextUtils
{
    companion object
    {
        fun isEmpty(str:CharSequence?):Boolean
        {
            return str == null || str.isEmpty()
        }
    }
}