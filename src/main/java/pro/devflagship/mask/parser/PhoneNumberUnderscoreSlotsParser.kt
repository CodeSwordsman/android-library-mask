package pro.devflagship.mask.parser

import pro.devflagship.mask.helpers.ConstHelper
import pro.devflagship.mask.slots.PredefinedSlots
import pro.devflagship.mask.slots.Slot
import pro.devflagship.mask.slots.SlotValidatorSet
import pro.devflagship.mask.slots.SlotValidators

class PhoneNumberUnderscoreSlotsParser : UnderscoreDigitSlotsParser() {
    var rule:Int = ConstHelper.ZERO_DIGIT

    override fun parseSlots(rawMask: CharSequence): Array<Slot> {
        rule = Slot.RULE_INPUT_MOVES_INPUT.or(Slot.RULE_INPUT_REPLACE)
        return super.parseSlots(rawMask)
    }

    override fun slotFromNonUnderscoredChar(character: Char): Slot {
        if (!character.isDigit()){
            rule = Slot.RULE_INPUT_MOVES_INPUT.or(Slot.RULE_INPUT_REPLACE)
            val hardcoded= PredefinedSlots.hardcodedSlot(character)
            return if (character == ConstHelper.PLUS_CHAR){hardcoded}else{hardcoded.withTags(Slot.TAG_DECORATION)}
        }
        val slot = Slot(rule, character, SlotValidatorSet.setOf(SlotValidators.Companion.DigitValidator()))
        rule = Slot.RULE_INPUT_MOVES_INPUT
        return slot
    }



}