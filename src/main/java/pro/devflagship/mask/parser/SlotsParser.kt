package pro.devflagship.mask.parser

import pro.devflagship.mask.slots.Slot

interface SlotsParser {
    fun parseSlots(rawMask: CharSequence) : Array<Slot>
}