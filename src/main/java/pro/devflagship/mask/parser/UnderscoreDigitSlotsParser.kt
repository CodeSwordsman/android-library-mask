package pro.devflagship.mask.parser

import android.text.TextUtils
import pro.devflagship.mask.helpers.ConstHelper
import pro.devflagship.mask.slots.PredefinedSlots
import pro.devflagship.mask.slots.Slot




open class UnderscoreDigitSlotsParser : SlotsParser {

    override fun parseSlots(rawMask: CharSequence): Array<Slot> {
        if(TextUtils.isEmpty(rawMask)){
            throw IllegalArgumentException(ConstHelper.REPRESENTATION_EMPTY_MESSAGE)
        }

        return Array(rawMask.length) { i -> slotFromChar(rawMask.elementAt(i))}
    }

    protected open fun slotFromChar(character:Char):Slot
    {
        if(character == ConstHelper.SPACE_CHAR)
        {
            return slotFromUnderscoreCharacter()
        }
        return slotFromNonUnderscoredChar(character)
    }
    protected open fun slotFromUnderscoreCharacter(): Slot
    {
        return PredefinedSlots.digit()
    }

    protected open fun slotFromNonUnderscoredChar(character: Char) : Slot
    {
        return PredefinedSlots.hardcodedSlot(character)
    }

}